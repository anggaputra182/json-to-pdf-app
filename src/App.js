import "./App.css";
import React, { useState } from "react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { lime, blue } from "@mui/material/colors";
import { BrowserRouter } from "react-router-dom";
import Routes from "../src/routes/index";

const theme = createTheme({
  palette: {
    primary: blue,
    secondary: blue,
  },
});

function App() {
  const [value, setValue] = useState("");

  const beautifyJson = () => {
    try {
      const beautifiedJson = JSON.stringify(JSON.parse(value), null, 2);
      setValue(beautifiedJson);
    } catch (error) {
      console.error("Invalid JSON format", error);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Routes />
      </BrowserRouter>
      {/* <div style={{ backgroundColor: "white", height: "100%" }}>
        <div
          style={{
            flexDirection: "row",
            display: "flex",
            backgroundColor: "blueviolet",
          }}
        >
          <div
            style={{
              width: "50vw",
              paddingLeft: "10px",
            }}
          >
            <h3>PDF Generator</h3>
            <p>Edit JSON and generate PDF</p>
          </div>

          <div
            style={{
              backgroundColor: "yellow",
              width: "50vw",
              paddingRight: "10px",
              paddingTop: "20px",
              justifyContent: "flex-end",
              display: "flex",
            }}
          >
            <div
              style={{
                backgroundColor: "gray",
                width: "35vw",
                height: "6vh",
                flexDirection: "row",
                justifyContent: "space-between",
                display: "flex",
                // flex: 1,
              }}
            >
              <Button
                variant="contained"
                style={{ color: "black", backgroundColor: "yellow" }}
                startIcon={<SettingsOutlinedIcon />}
              >
                Settings
              </Button>

              <Button
                variant="contained"
                color="secondary"
                startIcon={<AutoFixHighOutlinedIcon />}
              >
                Generate
              </Button>

              <Button
                variant="contained"
                color="secondary"
                startIcon={<DownloadForOfflineOutlinedIcon />}
              >
                Download
              </Button>
            </div>
          </div>
        </div>

        <div style={{ padding: "10px" }}>
          <Divider />
        </div>

        <div
          style={{
            flexDirection: "row",
            display: "flex",
            backgroundColor: "red",
            flex: 1,
          }}
        >
          <div
            style={{
              backgroundColor: "gray",
              width: "50vw",
              height: "82.5vh",
            }}
          >
            <div
              style={{
                backgroundColor: "yellow",

                justifyContent: "flex-end",
                display: "flex",
              }}
            >
              <Button
                variant="contained"
                color="secondary"
                onClick={beautifyJson}
              >
                beautify Json
              </Button>
            </div>

            <Textarea
              name="test-textarea"
              value={value}
              onValueChange={(value: string) => setValue(value)}
              numOfLines={28}
              placeholder="Enter JSON"
            />
          </div>

          <div
            style={{
              backgroundColor: "gray",
              width: "50vw",
              paddingLeft: "10px",
            }}
          >
            <h3>PDF Generator</h3>
            <p>Edit JSON and generate PDF</p>
          </div>
        </div>
      </div> */}
    </ThemeProvider>
  );
}

export default App;
