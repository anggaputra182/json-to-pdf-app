import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@mui/material/Box";
import {
  Button,
  TextField,
  Typography,
  InputAdornment,
  IconButton,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from "@material-ui/core";

import Http from "../helper/Fetch";
import { useParams, useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: theme.spacing(4),
  },
  form: {
    display: "flex",
    flexDirection: "column",
    width: "300px",
    margin: theme.spacing(2),
  },
  input: {
    marginBottom: theme.spacing(2),
  },
  tableHeader: {},
}));

const GeneratePdfTemplate = () => {
  const classes = useStyles();
  const history = useHistory();

  const [data, setData] = useState({
    Client_Information: {
      Client_Name: "Angga",
      Date_of_Birth: "1990-05-15",
      Gender: "Male",
      Marital_Status: "Single",
      Employment_Status: "Employed",
    },
    Financials: {
      Monthly_Income: "$5000",
      Monthly_Expenses: "$3500",
      Savings: "$10000",
      Investments: "$25000",
      Debts: "$5000",
      Cashflow: "$1500",
      Net_Worth: "$35000",
    },
    Goals: [
      {
        Goal: "Emergency Fund",
        Target_Amount: "$10000",
        Monthly_Contribution: "$500",
        Estimated_Duration: "20 months",
      },
      {
        Goal: "Travel",
        Target_Amount: "$5000",
        Monthly_Contribution: "$200",
        Estimated_Duration: "25 months",
      },
      {
        Goal: "Home Purchase",
        Target_Amount: "$150000",
        Monthly_Contribution: "$3000",
        Estimated_Duration: "50 months",
      },
    ],
    Insurances: [
      {
        Insurance_Type: "Life Insurance",
        Coverage_Amount: "$100000",
        Monthly_Premium: "$200",
        Provider: "ABC Insurance",
      },
      {
        Insurance_Type: "Health Insurance",
        Coverage_Amount: "$5000",
        Monthly_Premium: "$100",
        Provider: "XYZ Insurance",
      },
      {
        Insurance_Type: "Disability Insurance",
        Coverage_Amount: "$3000",
        Monthly_Premium: "$150",
        Provider: "PQR Insurance",
      },
    ],
  });

  const [isLoading, setisLoading] = useState(true);

  const getDataJSON = () => {
    setisLoading(true);
    Http.get(`api/Generate/Getdatalimit`)
      .then((res) => {
        console.log(res);
        if (res.status === 200) {
          console.log("res data : ", res?.data);
          setData(JSON.parse(res?.data?.json));
        }
      })
      .catch((e) => {
        if (e?.response) {
          // setMsg(e?.response?.data?.msg);
        }
        // setStateSnackbar({ ...stateSnackbar, open: true, msg: "Error" });
        // setisLoading(false);
        // console.log(e);
        // localStorage.clear();
      });
  };

  useEffect(() => {
    getDataJSON();
  }, []);

  const renderTable = (title, rows) => {
    title = title.replace(/_/g, " ");
    return (
      <div key={title}>
        <h2
          style={{
            backgroundColor: "rgb(168, 211, 255, 0.1)",
            borderTop: "1px solid rgba(0, 127, 255, 1)",
            borderBottom: "1px solid rgba(0, 127, 255, 1)",
            paddingTop: "10px",
            paddingBottom: "10px",
          }}
        >
          {title}
        </h2>
        <TableContainer component={Paper}>
          <Table aria-label={title}>
            <TableHead>
              <TableRow>
                {Object.keys(rows[0]).map((header, index) => (
                  <TableCell
                    style={{
                      backgroundColor: "white",
                    }}
                    key={index}
                  >
                    {header.replace(/_/g, " ")}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map((row, index) => (
                <TableRow
                  key={index}
                  style={{
                    backgroundColor: index % 2 === 0 ? "#f9f9f9" : "#ffffff",
                  }}
                >
                  {Object.values(row).map((cell, index) => (
                    <TableCell key={index}>{cell.replace(/_/g, " ")}</TableCell>
                  ))}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    );
  };

  const renderSection = (title, sectionData) => {
    title = title.replace(/_/g, " ");
    console.log("sectionData", title);
    if (Array.isArray(sectionData)) {
      return renderTable(title, sectionData);
    } else {
      return (
        <div key={title}>
          <Typography
            style={{
              fontWeight: "bold",
              backgroundColor: "rgb(168, 211, 255, 0.1)",
              padding: "10px",
              borderTop: "1px solid rgba(0, 127, 255, 1)",
              borderBottom: "1px solid rgba(0, 127, 255, 1)",
            }}
          >
            {title}
          </Typography>

          {Object.entries(sectionData).map(([key, value], idx) => (
            <>
              <div
                key={key}
                style={{
                  display: "flex",
                  flexDirection: "row",
                  backgroundColor: idx % 2 === 0 ? "#f9f9f9" : "#ffffff",
                  padding: "10px",
                  alignItems: "center",
                }}
              >
                <Typography style={{ width: "200px" }}>
                  {key.replace(/_/g, " ")}
                </Typography>

                <Typography>{`${value.replace(/_/g, " ")} `}</Typography>
              </div>
              {idx + 1 == Object.keys(sectionData).length && (
                <div
                  style={{ backgroundColor: "white", marginTop: "20px" }}
                ></div>
              )}
            </>
          ))}
        </div>
      );
    }
  };

  return (
    <div style={{ padding: "30px" }}>
      {Object.entries(data).map(([sectionTitle, sectionData]) =>
        renderSection(sectionTitle, sectionData)
      )}
    </div>
  );
};

export default GeneratePdfTemplate;
