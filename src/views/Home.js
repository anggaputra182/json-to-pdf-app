import "../App.css";
import React, { useState } from "react";
import { Textarea } from "../component/Textarea";
import {
  Button,
  Divider,
  LinearProgress,
  Box,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  FormControlLabel,
  Radio,
  RadioGroup,
  TextField,
  MenuItem,
  Select,
  InputLabel,
  FormControl,
  InputAdornment,
  IconButton,
  Typography,
} from "@material-ui/core";
import Alert from "@mui/material/Alert";
import SettingsOutlinedIcon from "@mui/icons-material/SettingsOutlined";
import DownloadForOfflineOutlinedIcon from "@mui/icons-material/DownloadForOfflineOutlined";
import CropPortraitOutlinedIcon from "@mui/icons-material/CropPortraitOutlined";
import SvgIcon from "@mui/material/SvgIcon";
import NoteOutlinedIcon from "@mui/icons-material/NoteOutlined";
// import { ImageIcon, LandscapeIcon } from "@mui/icons-material";
import CropLandscapeOutlinedIcon from "@mui/icons-material/CropLandscapeOutlined";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import AutoFixHighOutlinedIcon from "@mui/icons-material/AutoFixHighOutlined";
import { lime } from "@mui/material/colors";
import Http from "../helper/Fetch";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import PdfViewer from "../component/pdfViewer/PdfViewer";
import PDF from "../component/pdf/pdf";
import { Document, Page } from "react-pdf";

const theme = createTheme({
  palette: {
    primary: lime,
    secondary: lime,
  },
});

function Home() {
  const [value, setValue] = useState("");
  const [isLoading, setisLoading] = useState(false);
  const [open, setOpen] = useState(false);
  const [paperSize, setPaperSize] = useState("A4");
  const [isLandscape, setIsLandscape] = useState(false);
  const [marginTop, setMarginTop] = useState("0");
  const [marginBottom, setMarginBottom] = useState("0");
  const [marginLeft, setMarginLeft] = useState("0");
  const [marginRight, setMarginRight] = useState("0");
  const [fontType, setFontType] = useState("Arial");
  const [fontSize, setFontSize] = useState(12);
  const [showPassword, setShowPassword] = useState(false);
  const [isOpenPdf, setisOpenPdf] = useState(false);
  const [password, setPassword] = useState("");
  const [urlPdf, seturlPdf] = useState("");
  const [isValidJson, setIsValidJson] = useState(true);
  const [numPages, setNumPages] = useState(null);
  const [pageNumber, setPageNumber] = useState(1);

  const onDocumentLoadSuccess = (numPages) => {
    setNumPages(numPages);
    setisLoading(false);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const isValidJSON = (data) => {
    try {
      JSON.parse(data);
      return true;
    } catch (error) {
      return false;
    }
  };

  const apiPostJSON = async () => {
    setisOpenPdf(false);
    return new Promise((resolve) => {
      const isValid = isValidJSON(value);
      console.log("isValid = ", isValid);
      if (isValid) {
        try {
          const beautifiedJson = JSON.stringify(JSON.parse(value), null, 2);
          setValue(beautifiedJson);
        } catch (error) {
          console.error("Invalid JSON format", error);
        }

        setisLoading(true);
        setIsValidJson(isValid);
        let form_data = new FormData();

        form_data.append("JSON", value);
        console.log("formData : ", form_data);
        Http.post("api/Generate/add/1", form_data, {
          headers: { "Content-Type": "multipart/form-data" },
        })
          .then((response) => {
            console.log(response);
            setisOpenPdf(true);
            console.log("==========END FUNC UPLOAD=======");
            console.log("==========START FUNC REGISTER=======");
            handleButtonClick(2);
            // setisLoading(false);

            resolve("Sukses");
          })
          .catch((e) => {
            setisLoading(false);
            console.log(e);
          });
      } else {
        setIsValidJson(isValid);
      }
    });
  };

  const beautifyJson = () => {
    try {
      const beautifiedJson = JSON.stringify(JSON.parse(value), null, 2);
      setValue(beautifiedJson);
    } catch (error) {
      console.error("Invalid JSON format", error);
    }
  };

  const handleButtonClick = (state) => {
    setisLoading(true);

    // Get a reference to the iframe
    const iframe = document.getElementById("pdfIframe");
    if (state == 1) {
      seturlPdf(
        `https://localhost:7225/api/Generate/GeneratePdfV1?paperFormat=${paperSize}&marginTop=${marginTop}&marginBottom=${marginBottom}&marginLeft=${marginLeft}&marginRight=${marginRight}&fontType=${fontType}&fontSize=${fontSize}&landscape=${isLandscape}`
      );
      setisLoading(false);
    } else {
      seturlPdf(
        `https://localhost:7225/api/Generate/GeneratePdfv2?paperFormat=${paperSize}&marginTop=${marginTop}&marginBottom=${marginBottom}&marginLeft=${marginLeft}&marginRight=${marginRight}&fontType=${fontType}&fontSize=${fontSize}&landscape=${isLandscape}`
      );
    }

    // Reload the iframe by changing its src
    // iframe.src = urlPdf;
    iframe.file = urlPdf;
  };

  const downloadPDF = () => {
    // Make a GET request to the API endpoint that serves the PDF
    Http({
      url: `https://localhost:7225/api/Generate/GeneratePdfV1?paperFormat=${paperSize}&marginTop=${marginTop}&marginBottom=${marginBottom}&marginLeft=${marginLeft}&marginRight=${marginRight}&fontType=${fontType}&fontSize=${fontSize}&landscape=${isLandscape}`, // Replace with your API endpoint
      method: "GET",
      responseType: "blob", // Important: Response type must be set to 'blob' for binary data
    })
      .then((response) => {
        // Create a Blob from the PDF data
        const blob = new Blob([response.data], { type: "application/pdf" });

        // Create a link element
        const link = document.createElement("a");

        // Set the download attribute and file name
        link.href = window.URL.createObjectURL(blob);
        link.download = "document.pdf";

        // Append the link to the body
        document.body.appendChild(link);

        // Programmatically trigger the click event to start the download
        link.click();

        // Remove the link from the DOM
        document.body.removeChild(link);
      })
      .catch((error) => {
        console.error("Error downloading PDF:", error);
      });
  };

  React.useEffect(() => {
    console.log("isOpenPdf : ", isOpenPdf);
  }, [isOpenPdf]);

  return (
    <ThemeProvider theme={theme}>
      <div style={{ backgroundColor: "white", height: "100%" }}>
        {!isValidJson && <Alert severity="error">Invalid JSON!</Alert>}

        <Dialog open={open} onClose={handleClose}>
          <DialogTitle>Document Settings</DialogTitle>
          <DialogContent>
            <FormControl
              fullWidth
              variant="outlined"
              style={{ marginBottom: "1rem" }}
            >
              <InputLabel>Paper Size</InputLabel>
              <Select
                label="Paper Size"
                value={paperSize}
                onChange={(e) => setPaperSize(e.target.value)}
              >
                <MenuItem value="Letter">Letter</MenuItem>
                <MenuItem value="A6">A6</MenuItem>
                <MenuItem value="A5">A5</MenuItem>
                <MenuItem value="A4">A4</MenuItem>
                <MenuItem value="A3">A3</MenuItem>
                <MenuItem value="A2">A2</MenuItem>
                <MenuItem value="A1">A1</MenuItem>
                <MenuItem value="A0">A0</MenuItem>
              </Select>
            </FormControl>
            <FormControl component="fieldset" style={{ width: "100%" }}>
              <Typography variant="body2">Layout Orientation</Typography>
              <RadioGroup
                aria-label="Orientation"
                name="orientation"
                value={isLandscape}
                onChange={() => setIsLandscape(!isLandscape)}
                row
                style={{
                  // backgroundColor: "green",
                  display: "flex",
                  alignItems: "center",
                  width: "100%",
                  justifyContent: "space-between",
                }}
              >
                <div
                  style={{
                    display: "flex",
                    // alignItems: "center",
                    // justifyContent: "center",
                    // backgroundColor: "yellow",
                    width: "45%",
                    padding: "10px",
                    borderRadius: "10px",
                    border: "1px solid grey",
                  }}
                >
                  <FormControlLabel
                    value={false}
                    control={<Radio />}
                    // label="Portrait"
                  />
                  <div
                    style={{
                      // backgroundColor: "red",
                      display: "flex",
                      justifyContent: "center",
                      flexDirection: "column",
                      alignItems: "center",
                      padding: "10px",
                      marginLeft: "40px",
                    }}
                  >
                    <NoteOutlinedIcon
                      fontSize="large"
                      style={{
                        transform: "rotate(-90deg)",
                      }}
                    />
                    <Typography variant="caption">Portrait</Typography>
                  </div>
                </div>
                <div
                  style={{
                    display: "flex",
                    // alignItems: "center",
                    // justifyContent: "center",
                    // backgroundColor: "blueviolet",
                    width: "45%",
                    padding: "10px",
                    borderRadius: "10px",
                    border: "1px solid grey",
                  }}
                >
                  <FormControlLabel
                    value={true}
                    control={<Radio />}
                    // label=""
                  />

                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      flexDirection: "column",
                      alignItems: "center",
                      padding: "10px",
                      marginLeft: "40px",
                    }}
                  >
                    <NoteOutlinedIcon fontSize="large" />
                    <Typography variant="caption">Landscape</Typography>
                  </div>
                </div>
              </RadioGroup>
            </FormControl>
            <div
              style={{
                // backgroundColor: "red",
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                marginBottom: "1rem",
                marginTop: "1rem",
              }}
            >
              <TextField
                style={{ width: "49%" }}
                variant="outlined"
                label="Margin Top"
                type="number"
                value={marginTop}
                onChange={(e) => setMarginTop(e.target.value)}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="0.188889in"
                        height="0.188889in"
                        viewBox="0 0 35 35"
                      >
                        <path
                          id="Imported Path"
                          fill="none"
                          stroke="black"
                          d="M 29.18,6.20
           C 29.18,6.20 5.26,6.20 5.26,6.20M 33.33,30.35
           C 33.33,32.31 31.75,33.89 29.79,33.89
             29.79,33.89 4.65,33.89 4.65,33.89
             2.69,33.89 1.11,32.31 1.11,30.35
             1.11,30.35 1.11,5.21 1.11,5.21
             1.11,3.25 2.69,1.67 4.65,1.67
             4.65,1.67 29.79,1.67 29.79,1.67
             31.75,1.67 33.33,3.25 33.33,5.21
             33.33,5.21 33.33,30.35 33.33,30.35 Z"
                        />
                      </svg>
                    </InputAdornment>
                  ),
                }}
              />
              <TextField
                style={{ width: "49%" }}
                variant="outlined"
                label="Margin Bottom"
                type="number"
                value={marginBottom}
                onChange={(e) => setMarginBottom(e.target.value)}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="0.188889in"
                        height="0.188889in"
                        viewBox="0 0 35 35"
                      >
                        <path
                          id="Imported Path"
                          fill="none"
                          stroke="black"
                          // stroke-width="1"
                          d="M 5.26,29.36
           C 5.26,29.36 29.18,29.36 29.18,29.36M 1.11,5.21
           C 1.11,3.25 2.69,1.67 4.65,1.67
             4.65,1.67 29.79,1.67 29.79,1.67
             31.75,1.67 33.33,3.25 33.33,5.21
             33.33,5.21 33.33,30.35 33.33,30.35
             33.33,32.31 31.75,33.89 29.79,33.89
             29.79,33.89 4.65,33.89 4.65,33.89
             2.69,33.89 1.11,32.31 1.11,30.35
             1.11,30.35 1.11,5.21 1.11,5.21 Z"
                        />
                      </svg>
                    </InputAdornment>
                  ),
                }}
              />
            </div>

            <div
              style={{
                // backgroundColor: "red",
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                marginBottom: "1rem",
                marginTop: "1rem",
              }}
            >
              <TextField
                style={{ width: "49%" }}
                variant="outlined"
                label="Margin Left"
                type="number"
                value={marginLeft}
                onChange={(e) => setMarginLeft(e.target.value)}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="0.188889in"
                        height="0.188889in"
                        viewBox="0 0 35 35"
                      >
                        <path
                          id="Imported Path"
                          fill="none"
                          stroke="black"
                          // stroke-width="1"
                          d="M 5.64,5.82
           C 5.64,5.82 5.64,29.74 5.64,29.74M 29.79,1.67
           C 31.75,1.67 33.33,3.25 33.33,5.21
             33.33,5.21 33.33,30.35 33.33,30.35
             33.33,32.31 31.75,33.89 29.79,33.89
             29.79,33.89 4.65,33.89 4.65,33.89
             2.69,33.89 1.11,32.31 1.11,30.35
             1.11,30.35 1.11,5.21 1.11,5.21
             1.11,3.25 2.69,1.67 4.65,1.67
             4.65,1.67 29.79,1.67 29.79,1.67 Z"
                        />
                      </svg>
                    </InputAdornment>
                  ),
                }}
              />
              <TextField
                style={{ width: "49%" }}
                variant="outlined"
                label="Margin Right"
                type="number"
                value={marginRight}
                onChange={(e) => setMarginRight(e.target.value)}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="0.188889in"
                        height="0.188889in"
                        viewBox="0 0 35 35"
                      >
                        <path
                          id="Imported Path"
                          fill="none"
                          stroke="black"
                          d="M 28.80,29.74
           C 28.80,29.74 28.80,5.82 28.80,5.82M 29.79,1.67
           C 31.75,1.67 33.33,3.25 33.33,5.21
             33.33,5.21 33.33,30.35 33.33,30.35
             33.33,32.31 31.75,33.89 29.79,33.89
             29.79,33.89 4.65,33.89 4.65,33.89
             2.69,33.89 1.11,32.31 1.11,30.35
             1.11,30.35 1.11,5.21 1.11,5.21
             1.11,3.25 2.69,1.67 4.65,1.67
             4.65,1.67 29.79,1.67 29.79,1.67 Z"
                        />
                      </svg>
                    </InputAdornment>
                  ),
                }}
              />
            </div>

            <div
              style={{
                // backgroundColor: "red",
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                marginBottom: "1rem",
                marginTop: "1rem",
              }}
            >
              <FormControl style={{ width: "49%" }} variant="outlined">
                <InputLabel>Font Type</InputLabel>
                <Select
                  label="Font Type"
                  value={fontType}
                  onChange={(e) => setFontType(e.target.value)}
                >
                  <MenuItem value="Arial">Arial</MenuItem>
                  <MenuItem value="Times New Roman">Times New Roman</MenuItem>
                </Select>
              </FormControl>
              <FormControl style={{ width: "49%" }} variant="outlined">
                <InputLabel>Font Size</InputLabel>
                <Select
                  label="Font Size"
                  value={fontSize}
                  onChange={(e) => setFontSize(e.target.value)}
                >
                  <MenuItem value={12}>12</MenuItem>
                  <MenuItem value={14}>14</MenuItem>
                  <MenuItem value={16}>16</MenuItem>
                </Select>
              </FormControl>
            </div>

            <TextField
              fullWidth
              variant="outlined"
              label="Password"
              type={showPassword ? "text" : "password"}
              value={password}
              onChange={handlePasswordChange}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleShowPassword}
                    >
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />

            <div
              style={{
                // backgroundColor: "yellow",
                alignItems: "flex-end",
                display: "flex",
                justifyContent: "right",
              }}
            >
              <Button
                variant="contained"
                color="secondary"
                style={{
                  color: "white",
                  display: "flex",
                  backgroundColor: "blue",
                  marginTop: "1rem",
                }}
                onClick={() => {
                  seturlPdf(
                    `https://localhost:7225/api/Generate/GeneratePdfV1?paperFormat=${paperSize}&marginTop=${marginTop}&marginBottom=${marginBottom}&marginLeft=${marginLeft}&marginRight=${marginRight}&fontType=${fontType}&fontSize=${fontSize}&landscape=${isLandscape}`
                  );
                  setOpen(false);
                }}
                disabled={isLoading}
              >
                Save
              </Button>
            </div>
          </DialogContent>
        </Dialog>
        <div
          style={{
            flexDirection: "row",
            display: "flex",
            // backgroundColor: "yellow",
          }}
        >
          <div
            style={{
              width: "50vw",
              paddingLeft: "10px",
            }}
          >
            <h3>PDF Generator</h3>
            <p>Edit JSON and generate PDF</p>
          </div>

          <div
            style={{
              // backgroundColor: "blue",
              width: "50vw",
              paddingRight: "10px",
              paddingTop: "20px",
              justifyContent: "flex-end",
              display: "flex",
            }}
          >
            <div
              style={{
                // backgroundColor: "gray",
                width: "35vw",
                height: "6vh",
                flexDirection: "row",
                justifyContent: "space-between",
                display: "flex",
                // flex: 1,
              }}
            >
              <Button
                variant="contained"
                style={{ color: "black", backgroundColor: "white" }}
                startIcon={<SettingsOutlinedIcon />}
                onClick={() => handleClickOpen()}
                disabled={isLoading}
              >
                Settings
              </Button>

              <Button
                variant="contained"
                color="secondary"
                style={{ color: "black", backgroundColor: "white" }}
                onClick={() => apiPostJSON()}
                disabled={isLoading}
              >
                Generate
              </Button>

              <Button
                variant="contained"
                color="secondary"
                style={{ color: "white", backgroundColor: "blue" }}
                startIcon={<DownloadForOfflineOutlinedIcon />}
                onClick={() => downloadPDF()}
                disabled={isLoading}
              >
                Download
              </Button>
            </div>
          </div>
        </div>

        <div style={{ padding: "10px" }}>
          <Divider />
        </div>

        <div
          style={{
            flexDirection: "row",
            display: "flex",
            // backgroundColor: "gold",
            // flex: 1,
            padding: "10px",
          }}
        >
          <div
            style={{
              // backgroundColor: "gray",
              width: "50vw",
              height: "79vh",
            }}
          >
            {/* <div
              style={{
                backgroundColor: "white",
                justifyContent: "flex-end",
                display: "flex",
              }}
            >
              <Button
                style={{ color: "black", backgroundColor: "white" }}
                variant="contained"
                color="secondary"
                onClick={beautifyJson}
              >
                beautify Json
              </Button>
            </div> */}

            <Textarea
              name="test-textarea"
              value={value}
              onValueChange={(value: string) => setValue(value)}
              numOfLines={28}
              placeholder="Enter JSON"
            />
          </div>

          <div style={{ width: "20px", backgroundColor: "white" }}></div>
          <div
            style={{
              // backgroundColor: "gray",
              width: "50vw",
              height: "77.3vh",
              borderRadius: "25px",
              border: "1px solid grey",
            }}
          >
            <div
              style={{
                // backgroundColor: "yellow",
                overflow: "scroll",
                maxHeight: "77.3vh",
                overflowX: "hidden",
              }}
            >
              {isOpenPdf && (
                <PdfViewer
                  data={urlPdf}

                  // data={`https://localhost:7225/api/Generate/GeneratePdfV1?paperFormat=${paperSize}&marginTop=${marginTop}&marginBottom=${marginBottom}&marginLeft=${marginLeft}&marginRight=${marginRight}&fontType=${fontType}&fontSize=${fontSize}&landscape=${isLandscape}`}
                />
              )}
              {/* <PDF
                // data={urlPdf}
                data={`https://localhost:7225/api/Generate/GeneratePdfV1?paperFormat=${paperSize}&marginTop=${marginTop}&marginBottom=${marginBottom}&marginLeft=${marginLeft}&marginRight=${marginRight}&fontType=${fontType}&fontSize=${fontSize}&landscape=${isLandscape}`}
              /> */}
            </div>

            {/* <iframe
              id="pdfIframe"
              // src={"https://localhost:7225/api/Generate/GeneratePdfv2"}
              src={urlPdf}
              height="98.5%"
              width="100%"
              style={{
                width: "50vw",
                height: "78vh",
                borderRadius: "25px",
              }}
              onLoad={() => setisLoading(false)}
            /> */}
          </div>
        </div>
      </div>
    </ThemeProvider>
  );
}

export default Home;
