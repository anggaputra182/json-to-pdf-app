import Axios from "axios";
import Settings from "../settings";
// import Swal from 'sweetalert2';

const Http = Axios.create({
  baseURL: Settings.APIUrl,
  timeout: 100000,
  headers: { "Content-Type": "application/json" },
});

Http.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    // console.log(response)
    return response;
  },
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    if (
      error?.response?.status === 401 &&
      !window.location.href.includes("/home")
    ) {
      // Remove User
      // Swal.fire({
      //   text: "Your session has expired. Please log-in again",
      //   icon: "error",
      //   showConfirmButton: true,
      // }).then((result)=>{
      //   if (result.isConfirmed) {
      //     localStorage.clear();
      //     window.location.href = `/`
      //   }
      // })
    }
    return Promise.reject(error);
  }
);

export default Http;
