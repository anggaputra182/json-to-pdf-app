import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Cookies from "js-cookie";
import { Home, GeneratePdfTemplate } from "../views/index";
const Routes = () => {
  React.useEffect(() => {
    Object.keys(Cookies.get()).forEach(function (cookieName) {
      var neededAttributes = {
        // Here you pass the same attributes that were used when the cookie was created
        // and are required when removing the cookie
      };
      Cookies.remove(cookieName, neededAttributes);
    });
  }, []);
  return (
    <Switch>
      <Redirect exact from="/" to="/home" />
      <Route exact path="/home" component={Home} />
      <Route exact path="/generate" component={GeneratePdfTemplate} />
    </Switch>
  );
};
export default Routes;
