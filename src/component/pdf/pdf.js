import React, { useState } from "react";
import { Viewer, Worker } from "@react-pdf-viewer/core";
// import { defaultLayoutPlugin } from "@react-pdf-viewer/default-layout";
import PropTypes from "prop-types";

const PDF = (props) => {
  const { data } = props;
  const [pageNumber, setPageNumber] = useState(1);
  const [numPages, setNumPages] = useState(null);

  const onDocumentLoadSuccess = ({ numPages }) => {
    setNumPages(numPages);
  };

  return (
    <div>
      <div>
        <p>
          Page {pageNumber} of {numPages}
        </p>
      </div>
      <div>
        <Worker workerUrl="https://unpkg.com/pdfjs-dist/build/pdf.worker.js">
          <Viewer
            fileUrl={data}
            onLoadSuccess={onDocumentLoadSuccess}
            onPageLoadSuccess={({ pageIndex }) => setPageNumber(pageIndex + 1)}
          />
        </Worker>
      </div>
    </div>
  );
};

PDF.propTypes = {
  data: PropTypes.string.isRequired, // Assuming 'data' is a string
};

export default PDF;
