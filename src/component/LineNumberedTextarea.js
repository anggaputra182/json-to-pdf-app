import React, { useState, useRef, useEffect } from "react";
import "./LineNumberEditor.css";

const LineNumberEditor = () => {
  const [text, setText] = useState("");

  const handleChange = (event) => {
    setText(event.target.value);
  };

  const generateLineNumbers = () => {
    const lines = text.split("\n");
    return lines.map((_, index) => (
      <div key={index} className="line-number">
        {index + 1}
      </div>
    ));
  };

  const textareaRef = useRef();

  useEffect(() => {
    const handleScroll = () => {
      const scrollTop = textareaRef.current.scrollTop;
      textareaRef.current.previousSibling.scrollTop = scrollTop;
    };

    textareaRef.current.addEventListener("scroll", handleScroll);

    return () => {
      textareaRef.current.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <div className="editor-container">
      <div className="line-numbers">{generateLineNumbers()}</div>
      <textarea
        value={text}
        onChange={handleChange}
        className="code-editor"
        ref={textareaRef}
      />
    </div>
  );
};

export default LineNumberEditor;
