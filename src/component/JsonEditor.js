import React, { useState } from "react";
import LineNumber from "./LineNumber"; // Import the LineNumber component
// import "./JsonEditor.css";
import "./LineNumberEditor.css";

const JsonEditor = () => {
  const [jsonData, setJsonData] = useState("");

  //   const handleInputChange = (event) => {
  //     const value = event.target.value;
  //     setJsonData(value);
  //   };

  const beautifyJson = () => {
    try {
      const beautifiedJson = JSON.stringify(JSON.parse(jsonData), null, 2);
      setJsonData(beautifiedJson);
    } catch (error) {
      console.error("Invalid JSON format", error);
    }
  };

  // Calculate total lines in the text
  const totalLines = jsonData.split("\n").length;

  const [text, setText] = useState("");

  const handleInputChange = (event) => {
    const value = event.target.value;
    setText(value);
  };

  const generateLineNumbers = () => {
    return text.split("\n").map((_, index) => (
      <div key={index + 1} className="line-number">
        {index + 1}
      </div>
    ));
  };

  return (
    <div className="editor-container">
      <div className="line-numbers">{generateLineNumbers()}</div>
      <textarea
        value={text}
        onChange={handleInputChange}
        className="code-editor"
      />
    </div>
    // <div className="json-editor">
    //   {/* <LineNumber totalLines={totalLines} /> Add LineNumber component */}
    //   <textarea
    //     value={jsonData}
    //     onChange={handleInputChange}
    //     rows={10}
    //     cols={50}
    //     class="numbered"
    //   />
    //   <button onClick={beautifyJson}>Beautify JSON</button>
    //   {/* <div>
    //     <pre>{JSON.stringify(JSON.parse(jsonData), null, 2)}</pre>
    //   </div> */}
    // </div>
  );
};

export default JsonEditor;
