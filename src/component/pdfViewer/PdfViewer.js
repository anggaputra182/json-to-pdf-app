import React, { useState, useEffect, useCallback } from "react";
import { Document, Page, pdfjs } from "react-pdf";
// import { LinearProgress, Box } from "@material-ui/core";
import Box from "@mui/material/Box";
import LinearProgress from "@mui/material/LinearProgress";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import "./PdfViewer.css";
import PropTypes from "prop-types"; // Import PropTypes
import { useResizeObserver } from "@wojtekmaj/react-hooks";
// const throttle = require("lodash/throttle");

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

// pdfjs.GlobalWorkerOptions.workerSrc = new URL(
//   "pdfjs-dist/build/pdf.worker.min.js",
//   import.meta.url
// ).toString();
//source : https://levelup.gitconnected.com/displaying-pdf-in-react-app-6e9d1fffa1a9
//source : https://medium.com/better-programming/how-to-display-pdfs-but-prevent-them-from-downloading-in-react-2e77292ca9a5

const options = {
  cMapUrl: "/cmaps/",
  standardFontDataUrl: "/standard_fonts/",
};

const resizeObserverOptions = {};

const PdfViewer = (props) => {
  const { data, refPDF, setisOpenPdf } = props;
  const [numPages, setNumPages] = useState(null);

  function onDocumentLoadSuccess({ numPages }) {
    console.log("numPages : ", numPages);
    setNumPages(numPages);
  }

  function onDocumentLoadProgress({ loaded, total }) {
    console.log("numPages loaded : ", loaded);
    console.log("numPages total : ", total);
    // setprogress((loaded / total) * 100);
    // alert("Loading a document: " + (loaded / total) * 100 + "%");
  }

  const onProgress = (progressData) => {
    // setprogress((progressData.loaded / progressData.total) * 100);
  };

  const [progress, setProgress] = React.useState(0);

  React.useEffect(() => {
    const timer = setInterval(() => {
      setProgress((oldProgress) => {
        if (oldProgress === 100) {
          return 0;
        }
        const diff = Math.random() * 10;
        return Math.min(oldProgress + diff, 100);
      });
    }, 500);

    return () => {
      clearInterval(timer);
    };
  }, []);
  return (
    <div className="Example__container__document">
      <Document
        file={data}
        onLoadProgress={({ loaded, total }) =>
          console.log("Loading a document: " + (loaded / total) * 100 + "%")
        }
        onLoadSuccess={onDocumentLoadSuccess}
        options={options}
        loading={
          <div
            style={{
              backgroundColor: "rgba(122, 122, 122,0.1)",
              // display: "flex",
              width: "48vw",
              height: "77.3vh",
              borderRadius: "25px",
              display: "flex", // Uncomment this line
              justifyContent: "center", // Center horizontally
              alignItems: "center", // Center vertically.
              flexDirection: "column",
            }}
          >
            Generating PDF...
            <Box sx={{ width: "60%", marginTop: "20px" }}>
              <LinearProgress
                variant="determinate"
                value={progress}
                // aria-busy={true}
              />
            </Box>
          </div>
        }
      >
        {Array.from(new Array(numPages), (el, index) => (
          <Page key={`page_${index + 1}`} pageNumber={index + 1} />
        ))}
      </Document>
    </div>
    // <Document
    //   file={data}
    //   // onContextMenu={(e) => e.preventDefault()}
    //   onLoadSuccess={onDocumentLoadSuccess}
    //   options={options}
    //   className="pdf-container"
    //   loading="Loading File PDF"
    // >
    //   {/* <Page pageNumber={numPages} /> */}
    //   {Array.from(new Array(numPages), (el, index) => (
    //     <Page
    //       //   height={100}
    //       // renderMode="canvas"
    //       key={`page_${index + 1}`}
    //       pageNumber={index + 1}
    //       //   width={
    //       //     two
    //       //       ? 260
    //       //       : three
    //       //       ? 290
    //       //       : four
    //       //       ? 340
    //       //       : seven
    //       //       ? 440
    //       //       : oneHun
    //       //       ? 690
    //       //       : 1000
    //       //   }
    //     />
    //   ))}
    // </Document>
  );
};

PdfViewer.propTypes = {
  data: PropTypes.string.isRequired, // Assuming 'data' is a string
  refPDF: PropTypes.object.isRequired, // Assuming 'refPDF' is an object
  setisOpenPdf: PropTypes.func,
};

export default PdfViewer;
